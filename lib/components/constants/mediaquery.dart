import 'package:flutter/widgets.dart';

num mH(context) {
  return MediaQuery.of(context).size.height.toDouble();
}

num mW(context) {
  return MediaQuery.of(context).size.width.toDouble();
}
