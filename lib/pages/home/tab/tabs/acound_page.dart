import 'package:flutter/material.dart';

class AcoundPage extends StatefulWidget {
  const AcoundPage({super.key});

  @override
  State<AcoundPage> createState() => _AcoundPageState();
}

class _AcoundPageState extends State<AcoundPage> {
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text(
          'Acound page',
          style: TextStyle(
            fontSize: 32,
            fontWeight: FontWeight.bold,
            color: Colors.amber,
          ),
        ),
      ),
    );
  }
}
