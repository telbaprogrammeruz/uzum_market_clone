import 'package:flutter/material.dart';
import 'package:uzum_market_app/components/constants/image.dart';
import 'package:uzum_market_app/components/constants/mediaquery.dart';
import 'package:uzum_market_app/pages/home/tab/tabs/widgets/page_view_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: mW(context) * 0.5,
            // color: Colors.amber,
            child: PageView(
              scrollDirection: Axis.horizontal,
              children: [
                PageViewWidget(image: AppImages.pv_1),
                PageViewWidget(image: AppImages.pv_2),
                PageViewWidget(image: AppImages.pv_3),
                PageViewWidget(image: AppImages.pv_4),
              ],
            ),
          ),
          DefaultTabController(
            length: 3,
            child: Column(
              children: [
                const TabBar(tabs: [
                  Tab(
                    child: Text(
                      "Barakali juma",
                      style:
                          TextStyle(color: Color.fromARGB(255, 109, 45, 182)),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "Mashhur",
                      style:
                          TextStyle(color: Color.fromARGB(255, 109, 45, 182)),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "Yangi",
                      style:
                          TextStyle(color: Color.fromARGB(255, 109, 45, 182)),
                    ),
                  ),
                ]),
                Container(
                  padding: const EdgeInsets.all(6),
                  width: mW(context) * 0.9.toDouble(),
                  height: mW(context) * 1.05.toDouble(),
                  child: TabBarView(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: mW(context) * 0.8.toDouble(),
                            width: mW(context) * 0.425.toDouble(),
                            color: Colors.blue,
                            padding: const EdgeInsets.all(2),
                            child: Column(
                              children: [
                                Container(
                                  height: mW(context) * 0.5.toDouble(),
                                  width: mW(context) * 0.425.toDouble(),
                                  color: Colors.amber,
                                  // decoration: BoxDecoration(
                                  //   border: BorderRadius.circular(6),
                                  // ),
                                ), //rasm
                                const Text(
                                    "Grechka Mistral Yadricha 900 g"), // name
                              ],
                            ),
                          ),
                          Container(
                            height: mW(context) * 0.8.toDouble(),
                            width: mW(context) * 0.425.toDouble(),
                            color: Colors.green,
                          ),
                        ],
                      ),
                      Container(
                        height: mW(context) * 0.5.toDouble(),
                        width: mW(context).toDouble(),
                        color: Colors.amber,
                      ),
                      Container(
                        height: mW(context) * 0.5.toDouble(),
                        width: mW(context).toDouble(),
                        color: Colors.amber,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
