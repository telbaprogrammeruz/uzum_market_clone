import 'package:flutter/material.dart';
import '../../../../../components/constants/mediaquery.dart';

// ignore: must_be_immutable
class PageViewWidget extends StatelessWidget {
  String image;
  PageViewWidget({required this.image, super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: mW(context) * 0.5,
      width: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(image),
        ),
      ),
    );
  }
}
