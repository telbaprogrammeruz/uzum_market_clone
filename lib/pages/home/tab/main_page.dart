import 'package:flutter/material.dart';
import 'tabs/acound_page.dart';
import 'tabs/favs_page.dart';
import 'tabs/home_page.dart';
import 'tabs/search_page.dart';
import 'tabs/shopping_basket_page.dart';
import '../../../components/constants/mediaquery.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int currentPage = 0;

  @override
  Widget build(BuildContext context) {
    List pages = [
      const HomePage(),
      const SearchPage(),
      const ShoppingBasketPage(),
      const FavouritePage(),
      const AcoundPage(),
    ];

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        actions: [
          Center(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 16),
              width: mW(context) * 0.9,
              height: mW(context) * 0.08,
              decoration: BoxDecoration(
                color: Colors.grey.withOpacity(0.2),
                borderRadius: BorderRadius.circular(6),
              ),
              child: Center(
                child: Row(
                  children: const [
                    Center(
                      child: Icon(
                        Icons.search,
                        color: Colors.grey,
                      ),
                    ),
                    SizedBox(
                      width: 300,
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: "Maxsulot va toifalarni qidirish...",
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
      body: pages[currentPage],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentPage,
        onTap: (value) {
          setState(() {
            currentPage = value;
          });
        },
        type: BottomNavigationBarType.fixed,
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: "Bosh sahifa"),
          BottomNavigationBarItem(icon: Icon(Icons.search), label: "Qidirish"),
          BottomNavigationBarItem(
              icon: Icon(Icons.shopping_basket), label: "Savat"),
          BottomNavigationBarItem(
              icon: Icon(Icons.favorite_outline), label: "Istaklar"),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: "Kabinet"),
        ],
      ),
    );
  }
}
